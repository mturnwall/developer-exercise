# Developer Exercise

Author: **Michael Turnwall**

## Purpose

Create a basic photo ordering tool based on the images and JavaScript include provided. Thumbnails of the images should display in the order dictated by the include using a grid-based layout, and should be re-orderable by dragging the images to their desired position.

## Requirements

- This should be a completely client-side exercise. Server code to store the new photo order isn’t necessary.
- While the use of libraries is permitted for basic DOM manipulation or event handling, it’s expected that the core content should be original.
- Code should be optimized for Safari. Support for other browsers is not necessary.
- As a general rule, this exercise should take 2-3 hours.
- Extra points for style.